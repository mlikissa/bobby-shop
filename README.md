This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## To run this on your local machine
### `git clone https://gitlab.com/mlikissa/bobby-shop.git`
### `cd bobby-shop`
### `yarn/npm install`
### `yarn/npm start`



## Available Scripts

In the project directory, you can run:

### `npm start`

### `npm test`

### `npm run build`
