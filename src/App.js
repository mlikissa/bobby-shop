import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
//components
import Header from './components/Header'
import ProductGrid from './components/ProductGrid'
import ProductDetail from './components/ProductDetail'
import ShoppingCart from './components/ShoppingCart'

//context
import { ProductProvider } from './contexts/product-context';


function App() {
  return (
    <Router>
      <div>
        <ProductProvider>
          <Header />
          <Route exact path="/" component={ProductGrid} />
          <Route path="/detail" component={ProductDetail} />
          <Route path="/cart" component={ShoppingCart} />

        </ProductProvider>
      </div>
    </Router>
  );
}




export default App;
