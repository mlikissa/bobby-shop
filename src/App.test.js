import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import styled from 'styled-components'
import renderer from 'react-test-renderer'
import 'jest-styled-components'
import {shallow} from 'enzyme';

//Components
import Header from './components/Header'


it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});


describe('./components on App.js', ()=>{
  let wrapped;

  beforeEach(()=>{
    wrapped = shallow(<App />);
  })

  it('shoud  have a Header component',()=>{
    expect(wrapped.find(Header).length).toEqual(1);
  })
})





//testing styled components
describe('syteld components on App.js', ()=>{

  const Button = styled.button`
    background: transparent;
    border-radius: 3px;
    border: 2px solid palevioletred;
    color: palevioletred;
    margin: 0 1em;
    padding: 0.25em 1em;
    `

  it('should render a button', () => {
    const tree = renderer.create(<Button />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
