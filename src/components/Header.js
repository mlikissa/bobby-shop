import React from 'react';
import { Link } from "react-router-dom";
import styled from 'styled-components'
import Logo from '../assets/acme-logo.png'
import { ProductConsumer } from '../contexts/product-context';

import {
  Navbar,
  Container,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Badge
} from '@bootstrap-styled/v4';


const StyledContainer = styled(Container)`
  justify-content: space-between;
`

const StyledNavLink = styled(NavLink)`
  text-decoration:none;
`
const StyledNavbarBrand = styled(NavbarBrand)`
  text-decoration:none;
`

function Header() {
  return (
    <Navbar color="faded" light toggleable="xs">
      <StyledContainer>
        <div className="d-flex justify-content-between">
          <StyledNavbarBrand tag={Link} to="/"><img width="100" alt="Bobby Shop" src={Logo} /></StyledNavbarBrand>
        </div>
          <Nav navbar className="mr-auto">
            <NavItem>
              <StyledNavLink tag={Link} to="/">Home</StyledNavLink>
            </NavItem>
            <NavItem>
              <ProductConsumer>
                {({ cart }) => (
                  <StyledNavLink  tag={Link} to="/cart" disabled={cart.length===0}>Cart <Badge>{cart.length}</Badge></StyledNavLink>
                )}
                </ProductConsumer>

            </NavItem>
          </Nav>
      </StyledContainer>
    </Navbar>
  );
}

export default Header;
