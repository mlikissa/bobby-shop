import React from 'react';
import styled from 'styled-components'
import { ProductConsumer } from '../contexts/product-context';
import { Link } from "react-router-dom";
import {
  Card,
  CardImg,
  CardBlock,
  CardTitle
} from '@bootstrap-styled/v4';

const StyledCardTitle = styled(CardTitle)`
  color:#636c72;
`
const StyledCard = styled(Card)`
  cursor:pointer;
`


function ProductCard(props) {
  const card = props.card
    return (
      <ProductConsumer>
        {({ updateProduct }) => (
        <Link to={"/detail"} onClick={()=>updateProduct(card)}>
          <StyledCard>
            <CardImg width="100%"  top src={card.ProductImage.Link.Href} alt={card.Title} />
              <CardBlock>
                <StyledCardTitle>{card.Title}</StyledCardTitle>
              </CardBlock>
            </StyledCard>
          </Link>
        )}
        </ProductConsumer>

    );
}

export default ProductCard
