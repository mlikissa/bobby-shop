import React from 'react';
import styled,{css} from 'styled-components'
import { ProductConsumer } from '../contexts/product-context';

import "react-responsive-carousel/lib/styles/carousel.min.css";

const StyledDiv = styled.div`
  justify-content: center;
  align-items:center;
  display: flex;
  height: 100%;
  flex-direction:column;
`;
const StyledButton = styled.button`
  background: transparent;
  border-radius: 3px;
  border: 2px solid palevioletred;
  color: palevioletred;
  margin: 0 1em;
  padding: 0.25em 1em;

  ${props =>
  props.primary &&
  css`
    background: palevioletred;
    color: white;
  `};
`;



function ProductDescription(props) {

  const {
    description,
    addToCart
  } = props;


  return (
          <ProductConsumer>
            {({ selectedProduct }) =>
              <StyledDiv>
                <h3>{selectedProduct.Title}</h3>
                <p>{description}</p>
                <StyledButton primary onClick={()=>addToCart({selectedProduct})}>Add to Cart £ {selectedProduct.Price.Value} </StyledButton>
              </StyledDiv>
              }
          </ProductConsumer>
  );
}

export default ProductDescription;
