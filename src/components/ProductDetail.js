import React from 'react';
import { Redirect } from 'react-router-dom'

import ProductDescription from './ProductDescription'

import { ProductConsumer } from '../contexts/product-context';
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";

import {
  Container,
  Row,
  Col
} from '@bootstrap-styled/v4';




function ProductDetail() {

  return (
    <ProductConsumer>
        {({ selectedProduct,addToCart,cart }) =>

        Object.entries(selectedProduct).length === 0 && selectedProduct.constructor === Object?
        <Redirect to='/' />
        :
        <Container>
            <Row>
              <Col>
                <Carousel>
                  <div>
                      <img alt={selectedProduct.Title + " image 1"}src={selectedProduct.ProductImage.Link.Href}/>
                  </div>
                  {selectedProduct.ImageGallery.map(image =>{
                    return (<div>
                        <img alt={selectedProduct.Title + " image 1"}src={image}/>
                    </div>)
                  })
                  }
              </Carousel>
              </Col>
              <Col>

                <ProductDescription
                  description={selectedProduct.Description}
                  addToCart={addToCart}
                />
              </Col>
            </Row>
          </Container>
        }
      </ProductConsumer>
  );
}

export default ProductDetail;
