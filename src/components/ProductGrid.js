import React, { useState }  from 'react';
import ProductCard from './ProductCard'
import ProductData from '../data/cards.json'

import {
  Container,
  CardColumns,
} from '@bootstrap-styled/v4';

const Products = ProductData.Products;


function ProductGrid() {
  const [cards] = useState(Products);
  return (
      <Container>
        <CardColumns>
          {Object.keys(cards).map((card)=>{
            return  <ProductCard
              //handleShow={(e)=>setCard(cards[card])}
              key={cards[card].ProductId}
              card={cards[card]}
            />
          })}
        </CardColumns>
      </Container>
  );
}

export default ProductGrid;
