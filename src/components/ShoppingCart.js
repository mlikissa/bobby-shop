import React  from 'react';
import { Redirect } from 'react-router-dom'
import StyledButton from './StyledButton'
//context
import { ProductConsumer } from '../contexts/product-context';
//components
//bootstrap
import {
  Container,
  Table,
  Thead,
  Tr,
  Th,
  Tbody,
  Td,
  } from '@bootstrap-styled/v4';



function ShoppingCart() {


  return (
      <Container>
        <Table>
          <Thead>
            <Tr>
              <Th>#</Th>
              <Th>Image</Th>
              <Th>Name</Th>
              <Th>Quantity</Th>
              <Th></Th>
            </Tr>
          </Thead>

          <ProductConsumer>
            {({cart,addCartItem,subtractCartItem,removeCartItem }) =>
              cart.length === 0?
                <Redirect to='/' />
                :
                <Tbody>{
                  cart.map((item,key) =>{
                    return(<Tr key={item.ProductId}>
                      <Th scope="row">{key + 1}</Th>
                      <Td><img alt={item.Title + " image 2"} src={item.ProductImage.Link.Href} width="50" /></Td>
                      <Td>{item.Title}</Td>
                      <Td><StyledButton onClick={()=>subtractCartItem(item)}  disabled={item.Quantity=== 1}>-</StyledButton>{item.Quantity}<StyledButton onClick={()=>addCartItem(item)}>+</StyledButton></Td>
                      <Td>
                          <StyledButton warning onClick={()=>removeCartItem(item)}>Remove</StyledButton>
                      </Td>
                    </Tr>)
                  })
                }</Tbody>
            }
          </ProductConsumer>


      </Table>

      </Container>
  );
}

export default ShoppingCart;
