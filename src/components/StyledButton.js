import styled, {css} from 'styled-components'


const StyledButton = styled.button`
  background: transparent;
  border-radius: 3px;
  border: 2px solid palevioletred;
  color: palevioletred;
  margin: 0 1em;
  padding: 0.25em 1em;

  :disabled{
    border: 2px solid #eaeaea;
  }
  ${props =>
  props.primary &&
  css`
    background: palevioletred;
    color: white;
  `};
  ${props =>
  props.warning &&
  css`
    border: 2px solid #f5c6cb;
    background: #f8d7da;
    color: #721c24;
  `};
`


export default StyledButton
