import React from 'react';
import ReactDOM from 'react-dom';
import Header from '../Header';
import styled from 'styled-components'
import renderer from 'react-test-renderer'
import 'jest-styled-components'

import {
  A,
  Navbar,
  Container,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from '@bootstrap-styled/v4';


//testing styled components inc bootstrap
describe('syteld components on Header.js', ()=>{

  const StyledContainer = styled(Container)`
  justify-content: space-between;
`
  it('should render a StyledContainer', () => {
    const tree = renderer.create(<StyledContainer />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render a Navbar', () => {
    const tree = renderer.create(<Navbar />).toJSON()
    expect(tree).toMatchSnapshot()
  })
  it('should render a NavbarBrand', () => {
    const tree = renderer.create(<NavbarBrand />).toJSON()
    expect(tree).toMatchSnapshot()
  })
  it('should render a Nav with Nave Items that contian Nab Links', () => {
    const tree = renderer.create(<Nav><NavItem><NavLink /></NavItem></Nav>).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

test.todo('should test router Links are valid');
test.todo('test state change toggles disabled on cart ');
