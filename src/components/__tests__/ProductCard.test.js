import React from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components'
import renderer from 'react-test-renderer'
import ProductCard from '../ProductCard.js'
import 'jest-styled-components'

import {
  Card,
  CardImg,
  CardBlock,
  CardTitle
} from '@bootstrap-styled/v4';


//testing styled components inc bootstrap
describe('syteld components on ProductCard.js', ()=>{

  it('should render a Container', () => {
    const tree = renderer.create(<Card />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render a CardImg', () => {
    const tree = renderer.create(<CardImg />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render a CardBlock', () => {
    const tree = renderer.create(<CardBlock />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render a CardTitle', () => {
    const tree = renderer.create(<CardTitle />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
