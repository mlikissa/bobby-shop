import React from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components'
import renderer from 'react-test-renderer'
import ProductGrid from '../ProductGrid.js';
import ProductCard from '../ProductCard.js'
import 'jest-styled-components'
import {shallow} from 'enzyme';

import {
  Container,
  CardColumns,
} from '@bootstrap-styled/v4';


//testing styled components inc bootstrap
describe('syteld components on ProductGrid.js', ()=>{

  it('should render a Container', () => {
    const tree = renderer.create(<Container />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render a CardColumns', () => {
    const tree = renderer.create(<CardColumns />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('./components on ProductGrid.js', ()=>{
  let wrapped;

  beforeEach(()=>{
    wrapped = shallow(<ProductGrid />);
  })

  it('shoud  have Product Cards displayed',()=>{
    expect(wrapped.find(ProductCard).length).toBeGreaterThan(0);
  })
})

test.todo('should test handleShow action');
test.todo('should cope with 0 products');
