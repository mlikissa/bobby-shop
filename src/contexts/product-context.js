import React, { createContext } from 'react';

const ProductContext = createContext({
  selectedProduct: {},
  updateProduct: () => {},
  cart:[],
  addToCart: () => {},
  addCartItem:()=>{},
  subtractCartItem:()=>{},
  removeCartItem:()=>{}
});

export class ProductProvider extends React.Component {
  updateProduct = newProduct => {
    this.setState({ selectedProduct: newProduct });
  };

  addCartItem = cartItem => {
    let index = this.state.cart.findIndex(el => el.ProductId === cartItem.ProductId);
    this.setState(prevState => {
      let newCart = [...prevState.cart];
      newCart[index].Quantity += 1;
      return {cart: newCart};
    });
  }
  subtractCartItem = cartItem => {
    let index = this.state.cart.findIndex(el => el.ProductId === cartItem.ProductId);
    this.setState(prevState => {
      let newCart = [...prevState.cart];
      newCart[index].Quantity += -1;
      return {cart: newCart};
    });
  }
  removeCartItem = cartItem => {
    let index = this.state.cart.findIndex(el => el.ProductId === cartItem.ProductId);
    this.setState(prevState => {
      let newCart = [...prevState.cart];
      newCart.splice(index, 1);
      return {cart: newCart};
    });
  }

  addToCart = () => {
    const selectedProduct = this.state.selectedProduct;
    let index = this.state.cart.findIndex(el => el.ProductId === selectedProduct.ProductId);
    if (index === -1) {
        this.setState(prevState => ({
        cart: [
            ...prevState.cart,
            {
              ProductId: selectedProduct.ProductId,
              ProductImage:{Link:{Href:selectedProduct.ProductImage.Link.Href}},
              Title: selectedProduct.Title,
              Price: {Value:selectedProduct.Price.Value},
              Quantity: 1
            }
          ]
      }));
    }else {
      this.setState(prevState => {
        let newCart = [...prevState.cart];
        newCart[index].Quantity += 1;
        return {cart: newCart};
      });
    }
  }


  state = {
    selectedProduct: {},
    updateProduct: this.updateProduct,
    cart:[],
    addToCart:this.addToCart,
    addCartItem:this.addCartItem,
    subtractCartItem:this.subtractCartItem,
    removeCartItem:this.removeCartItem
  };

  render() {
    return (
      <ProductContext.Provider value={this.state} {...this.props}>

        {this.props.children}
      </ProductContext.Provider>
    );
  }
}

export const ProductConsumer = ProductContext.Consumer;
